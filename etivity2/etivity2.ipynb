{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Student Name**: Phil Miesle\n",
    "\n",
    "**Student ID**: 23287012"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "from sklearn import manifold #needed for multidimensional scaling (MDS) and t-SNE\n",
    "from sklearn import cluster #needed for k-Means clustering\n",
    "from sklearn.preprocessing import MinMaxScaler, StandardScaler, RobustScaler, FunctionTransformer #needed for data preparation\n",
    "\n",
    "from sklearn.pipeline import Pipeline, make_pipeline\n",
    "from sklearn.compose import ColumnTransformer\n",
    "from sklearn import set_config"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Task 1: Data Preparation Pipeline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Open a new Jupyter notebook and name it etivity2.ipynb. In this notebook, create a data preparation pipeline that applies the same kind of transformations that you applied as part of e-tivity 1. It is OK to leave some of the transformations outside the pipeline but aim at including as many transformations as you can within the pipeline. Follow the notebook Tutorial 2 - Clustering and Manifold Learning.ipynb as an example."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Loading Data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = pd.read_csv('./bank.csv')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's also try to do this the \"right way\", and split the data into test and training data frames."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.model_selection import train_test_split\n",
    "df_train, df_test = train_test_split(df, test_size=0.2, random_state=1618)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Missing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Code for filling in missing data was previously:\n",
    "\n",
    "```python\n",
    "df['job'] = df['job'].fillna('unknown')\n",
    "df['education'] = df['education'].fillna('unknown')\n",
    "df['contact'] = df['contact'].fillna('unknown')\n",
    "\n",
    "df.loc[df['pdays'] == -1, 'poutcome'] = df.loc[df['pdays'] == -1, 'poutcome'].fillna('no_previous')\n",
    "median_age = df['age'].median()\n",
    "df['age'] = df['age'].fillna(median_age)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Filling in with unknowns is relatively straightforward, as is the median age. We will be able to use the `SimpleImputer` for these."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.impute import SimpleImputer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I did spot in the docs the existance of a [`KNNImputer`](https://scikit-learn.org/stable/modules/generated/sklearn.impute.KNNImputer.html) which might be a better choice for `age` but for the time being I will stick to the brief \"apply same kinds of transforms as in etivity 1.\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `poutcome` is a bit trickier, as it's conditional on another column. There seems to be an [`IterativeImputer`](https://scikit-learn.org/stable/modules/generated/sklearn.impute.IterativeImputer.html) but from what I can work out it's definitely overkill and seems to be used when you have some sort of model-type estimator. It's also experimental...which given the interface / how-to-use is not a surprise.\n",
    "\n",
    "So there looks to be a way one can define your own imputer, for example explained on [Stack Overflow](https://stackoverflow.com/questions/61278575/create-my-custom-imputer-for-categorical-variables-sklearn) and then in the [docs](https://scikit-learn.org/stable/developers/develop.html):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.base import BaseEstimator, TransformerMixin\n",
    "\n",
    "class EqualityConditionImputer(BaseEstimator, TransformerMixin):\n",
    "    def __init__(self, fillna_col: str, condition_col: str, condition_val: int, fill_value:str):\n",
    "        self.fillna_col = fillna_col\n",
    "        self.condition_col = condition_col\n",
    "        self.condition_val = condition_val\n",
    "        self.fill_value = fill_value\n",
    "\n",
    "    def fit(self, X, y=None):\n",
    "        # As there is nothing to specifically 'fit', this is effectively a null op.\n",
    "        return self\n",
    "\n",
    "    def transform(self, X):\n",
    "        # Ensure working on a copy to avoid changing the original DataFrame\n",
    "        X = X.copy()\n",
    "\n",
    "        condition = X[self.condition_col] == self.condition_val\n",
    "        X.loc[condition, self.fillna_col] = X.loc[condition, self.fillna_col].fillna(self.fill_value)\n",
    "        return X"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Binning"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For `age` and `day` (and a few others), I'd identified some manually-defined bin boundaries. [`KBinsDiscretizer`](https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.KBinsDiscretizer.html) looks to have some capabilities around this, but in my code I'd manually defined bins:\n",
    "\n",
    "```python\n",
    "age_bins = [0, 26, 36, 46, 56, 66, 150] # 0-25, 26-35, ...\n",
    "age_labels = [0, 1, 2, 3, 4, 5]  # Assigning labels to each bin\n",
    "df['age_bin'] = pd.cut(df['age'], bins=age_bins, labels=age_labels, right=False)\n",
    "\n",
    "day_bins = [0, 11, 21, 32]\n",
    "day_labels = [0, 1, 2] \n",
    "df['day_bin'] = pd.cut(df['day'], bins=day_bins, labels=day_labels, right=False)\n",
    "```\n",
    "\n",
    "And then one-shot encoded these. I'll create a pipeline component that does this binning, and then one-shot encode everything in a subsequent step; similar to `EqualityConditionImputer`, I'll need a custom transformer for this job it seems!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [],
   "source": [
    "class BinningTransformer(BaseEstimator, TransformerMixin):\n",
    "    def __init__(self, bins: list[int], labels: list[int], right=False):\n",
    "        self.bins = bins\n",
    "        self.labels = labels\n",
    "        self.right = right\n",
    "\n",
    "    def fit(self, X, y=None):\n",
    "        return self\n",
    "\n",
    "    def transform(self, X):\n",
    "        X = X.copy()\n",
    "        for col in X.columns:\n",
    "            # cut is really nifty!\n",
    "            # https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.cut.html\n",
    "            X[col] = pd.cut(X[col], bins=self.bins, labels=self.labels, right=self.right)\n",
    "        return X"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Scale + Log "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I had a number of steps that did a scale-and-log, similar to:\n",
    "\n",
    "```python\n",
    "df[['balance_scaled']] = robust_scaler.fit_transform(df[['balance']])\n",
    "df['balance_scaled_log'] = np.log(df['balance_scaled'] + abs(min(df['balance_scaled'])) + 1)\n",
    "```\n",
    "\n",
    "and whereas the example notebook used `FunctionTransformer(func=np.log)`, I know there are some negative values here which means it can't be used...so let me make yet another custom transformer to handle that step, and then put `balance` and `duration` into that."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [],
   "source": [
    "class AddLogTransformer(BaseEstimator, TransformerMixin):\n",
    "    def __init__(self, add_amt:int=1):\n",
    "        self.add_amt = add_amt\n",
    "\n",
    "    def fit(self, X, y=None):\n",
    "        # scikitlearn convention when adding attributes says to name with trailing _\n",
    "        # https://scikit-learn.org/stable/developers/develop.html#estimated-attributes\n",
    "        self.min_to_add_ = np.abs(np.min(X, axis=0)) + self.add_amt\n",
    "        return self\n",
    "\n",
    "    def transform(self, X):\n",
    "        X = X.copy()\n",
    "        X += self.min_to_add_\n",
    "        return np.log(X)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Clipping"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I apply clipping in a few places...yet again there's no pre-defined transformer I could find...\n",
    "\n",
    "```python\n",
    "df['campaign_category'] = df['campaign'].clip(upper=4)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [],
   "source": [
    "class ClippingTransformer(BaseEstimator, TransformerMixin):\n",
    "    def __init__(self, upper=None):\n",
    "        self.upper = upper\n",
    "\n",
    "    def fit(self, X, y=None):\n",
    "        return self\n",
    "\n",
    "    def transform(self, X):\n",
    "        X = X.copy()\n",
    "        if self.upper is not None:\n",
    "            X = X.clip(upper=self.upper)\n",
    "        return X"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Mapping"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There looks to be a couple ways to do mapping, and having done (maybe too many!) custom functions let me try a different technique."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [],
   "source": [
    "def mapper(X, from_val:any, to_val:any):\n",
    "    X=X.copy()\n",
    "    X = np.where(X == from_val, to_val, X)\n",
    "    return X"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Revisiting `previous` and `pdays`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These two features are now processed to the point they are ready for binning, but need to bear in mind that the binning pipeline will have to run after the clipping and mapping steps in the final transformation sequence."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Calculated Feature"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I had created a \"Balance-to-Age ratio\" feature which was then scaled:\n",
    "\n",
    "```python\n",
    "df['balance_age_ratio'] = df['balance'] / df['age']\n",
    "df[['balance_age_ratio_scaled']] = robust_scaler.fit_transform(df[['balance_age_ratio']])\n",
    "df['balance_age_ratio_scaled_log'] = np.log(df['balance_age_ratio_scaled'] + abs(min(df['balance_age_ratio_scaled'])) + 1)\n",
    "```\n",
    "\n",
    "Seems we need yet another custom transformer to make this happen!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {},
   "outputs": [],
   "source": [
    "class ColumnRatioTransformer(BaseEstimator, TransformerMixin):\n",
    "    def __init__(self, numerator_col, denominator_col, result_col):\n",
    "        self.numerator_col = numerator_col\n",
    "        self.denominator_col = denominator_col\n",
    "        self.result_col = result_col\n",
    "\n",
    "    def fit(self, X, y=None):\n",
    "        return self\n",
    "\n",
    "    def transform(self, X):\n",
    "        ratio = (X[self.numerator_col] / X[self.denominator_col]).to_frame(name=self.result_col)\n",
    "        return ratio\n",
    "\n",
    "# Keep the new feature encapsulated into a single pipeline step, allowing a more atomic operation\n",
    "balance_age_ratio_pipeline = Pipeline([\n",
    "    ('create_ratio', ColumnRatioTransformer('balance', 'age', 'balance_age_ratio')), \n",
    "    ('robust_scaling', RobustScaler()),\n",
    "    ('add_log', AddLogTransformer(add_amt=1)),\n",
    "])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dropping Column"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I may be missing something, but the idea of \"drop a column\" as part of the pipeline doesnt' seem to be a built-in capability? So following from [this example](https://stackoverflow.com/questions/68402691/adding-dropping-column-instance-into-a-pipeline), instead of using `remainder='drop'`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 59,
   "metadata": {},
   "outputs": [],
   "source": [
    "class ColumnDropperTransformer(BaseEstimator, TransformerMixin):\n",
    "    def __init__(self,columns):\n",
    "        self.columns=columns\n",
    "\n",
    "    def transform(self,X,y=None):\n",
    "        return X.drop(self.columns,axis=1)\n",
    "\n",
    "    def fit(self, X, y=None):\n",
    "        # this is now the null op!\n",
    "        return self "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Binary Columns"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I'd previously mapped the `yes/no` columns to a `bool` and then to a `1/0` value. We can do similar here..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [],
   "source": [
    "def yes_no_to_numeric(X):\n",
    "    X = X.copy()\n",
    "    X = np.where(X == 'yes', 1, X)\n",
    "    X = np.where(X == 'no', 0, X)\n",
    "    return X\n",
    "\n",
    "# binary_transformer = FunctionTransformer(yes_no_to_numeric)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## One-Shot Encoding"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we have columns that need to be one-shot encoded, including some of the columns that are present in a pipeline above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.preprocessing import OneHotEncoder\n",
    "\n",
    "# onehot_encoder = OneHotEncoder(sparse_output=False, handle_unknown='ignore')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Final Transform Pipeline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 62,
   "metadata": {},
   "outputs": [],
   "source": [
    "pdays_max=730\n",
    "\n",
    "all_transformations = ColumnTransformer([\n",
    "    # Imputation\n",
    "    ('fill_unknown', SimpleImputer(strategy='constant', fill_value='unknown'), ['job', 'education', 'contact']),\n",
    "    ('fill_median', SimpleImputer(strategy='median'), ['age']),\n",
    "    ('fill_poutcome', EqualityConditionImputer(fillna_col='poutcome', condition_col='pdays', condition_val=-1, fill_value='no_previous'), ['poutcome', 'pdays']),\n",
    "\n",
    "    # Scaling and Log  \n",
    "    ('robust_scaling', RobustScaler(), ['balance', 'duration']),\n",
    "    ('add_log', AddLogTransformer(add_amt=1), ['balance', 'duration']),\n",
    "\n",
    "    # Clipping\n",
    "    ('campaign_clipping', ClippingTransformer(upper=4), ['campaign']),\n",
    "    ('previous_clipping', ClippingTransformer(upper=10), ['previous']),\n",
    "    ('pdays_clipping', ClippingTransformer(upper=pdays_max), ['pdays']),\n",
    "\n",
    "    # Mapping\n",
    "    ('pdays_mapping', FunctionTransformer(func=mapper,kw_args={'from_val': -1, 'to_val': pdays_max}), ['pdays']),\n",
    "\n",
    "    # Adding Columns\n",
    "    ('balance_age_ratio', balance_age_ratio_pipeline, ['balance', 'age']),\n",
    "\n",
    "    # Binning\n",
    "    ('bin_age', BinningTransformer(\n",
    "        bins=[0, 26, 36, 46, 56, 66, 150], \n",
    "        labels=[0, 1, 2, 3, 4, 5]), \n",
    "    ['age']),\n",
    "    ('bin_day', BinningTransformer(\n",
    "        bins=[0, 11, 21, 32], \n",
    "        labels=[0, 1, 2]), \n",
    "    ['day']),\n",
    "    ('bin_previous', BinningTransformer(\n",
    "        bins=[0, 5, 8, np.inf], \n",
    "        labels=[0, 1, 2]), \n",
    "    ['previous']),\n",
    "    ('bin_pdays', BinningTransformer(\n",
    "        bins=[-np.inf, 91, 182, 283, 365, 729, np.inf], \n",
    "        labels=['0-91', '92-182', '183-273', '274-365', '366-729', '730+']), \n",
    "    ['pdays']),\n",
    "\n",
    "    # Binary transformation\n",
    "    ('yes_no_to_numeric', FunctionTransformer(yes_no_to_numeric), ['housing', 'default', 'loan', 'subscribed']),\n",
    "\n",
    "    # One-hot encoding\n",
    "    ('onehot_encoding', OneHotEncoder(sparse_output=False, handle_unknown='ignore'), ['job', 'marital', 'education', 'contact', 'poutcome']),\n",
    "\n",
    "    # Manually drop columns\n",
    "    ('drop_columns', ColumnDropperTransformer(['month']), ['month']),\n",
    "\n",
    "], remainder='passthrough') "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Fitting"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A simple/small dataframe will assist in testing..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 63,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[['admin' 'high.school' None 25.0 'no_previous' -1 0.6666666666666666\n",
      "  1.5510204081632653 7.313886831633462 6.111467339502679 1 0 -1 730 nan\n",
      "  0.0 0 0 '0-91' 1 0 0 1 1.0 0.0 0.0 0.0 0.0 0.0 1.0 0.0 0.0 1.0 0.0 0.0\n",
      "  0.0 0.0 1.0 0.0 0.0 0.0 1.0]\n",
      " ['teacher' 'university.degree' 'cellular' 35.0 'success' 184\n",
      "  -0.5833333333333334 -0.32653061224489793 0.0 5.3981627015177525 3 1 184\n",
      "  184 nan 1.0 0 0 '183-273' 0 0 1 0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 0.0\n",
      "  0.0 1.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 0.0]\n",
      " ['unknown' 'unknown' 'telephone' 32.5 'failure' 365 0.0 nan\n",
      "  6.55250788703459 nan 2 2 365 365 nan nan 1 0 '366-729' 1 1 0 1 0.0 0.0\n",
      "  0.0 0.0 1.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 0.0 1.0 0.0 1.0 0.0 0.0 0.0]\n",
      " ['blue-collar' 'basic.9y' 'cellular' 45.0 'nonexistent' 999\n",
      "  41.333333333333336 0.32653061224489793 10.825780236605917\n",
      "  5.707110264748875 2 0 730 999 nan 2.0 1 0 '730+' 0 0 0 0 0.0 1.0 0.0\n",
      "  0.0 0.0 1.0 0.0 0.0 1.0 0.0 0.0 0.0 1.0 0.0 0.0 0.0 1.0 0.0 0.0]\n",
      " ['technician' 'high.school' 'telephone' 30.0 'failure' 10\n",
      "  -0.3333333333333333 -0.4897959183673469 5.707110264748875\n",
      "  5.303304908059076 4 1 10 10 nan 1.0 2 0 '0-91' 1 0 1 0 0.0 0.0 0.0 1.0\n",
      "  0.0 0.0 1.0 0.0 0.0 1.0 0.0 0.0 0.0 1.0 0.0 1.0 0.0 0.0 0.0]]\n"
     ]
    }
   ],
   "source": [
    "unit_test_data = pd.DataFrame({\n",
    "    'age': [25, 35, np.nan, 45, 30],\n",
    "    'job': ['admin', 'teacher', np.nan, 'blue-collar', 'technician'],\n",
    "    'marital': ['married', 'single', 'married', 'divorced', 'married'],\n",
    "    'education': ['high.school', 'university.degree', np.nan, 'basic.9y', 'high.school'],\n",
    "    'default': ['no', 'no', 'yes', 'no', 'no'],\n",
    "    'balance': [1200, -300, 400, 50000, 0],\n",
    "    'housing': ['yes', 'no', 'yes', 'no', 'yes'],\n",
    "    'loan': ['no', 'yes', 'no', 'no', 'yes'],\n",
    "    'contact': [None, 'cellular', 'telephone', 'cellular', 'telephone'],\n",
    "    'day': [5, 10, 15, 20, 25],\n",
    "    'month': ['jan', 'feb', 'mar', 'apr', 'may'],\n",
    "    'duration': [350, 120, None, 200, 100],\n",
    "    'campaign': [1, 3, 2, 2, 4],\n",
    "    'pdays': [-1, 184, 365, 999, 10],\n",
    "    'previous': [0, 1, 2, 0, 1],\n",
    "    'poutcome': [None, 'success', 'failure', 'nonexistent', 'failure'],\n",
    "    'subscribed': ['yes', 'no', 'yes', 'no', 'no']\n",
    "})\n",
    "\n",
    "# Applying the pipeline\n",
    "processed_data = all_transformations.fit_transform(unit_test_data)\n",
    "print(processed_data)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 64,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[['housemaid' 'primary' 'cellular' ... 0.0 0.0 0.0]\n",
      " ['services' 'secondary' 'cellular' ... 0.0 0.0 0.0]\n",
      " ['self-employed' 'secondary' 'cellular' ... 0.0 0.0 0.0]\n",
      " ...\n",
      " ['management' 'tertiary' 'cellular' ... 0.0 0.0 0.0]\n",
      " ['technician' 'secondary' 'cellular' ... 0.0 0.0 0.0]\n",
      " ['self-employed' 'secondary' 'cellular' ... 1.0 0.0 0.0]]\n"
     ]
    }
   ],
   "source": [
    "transformed_train_df = all_transformations.fit_transform(df_train)\n",
    "print(transformed_train_df)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Task 2: k-Means"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Plot the sum of squared distances from the data points in the prepared bank dataset to the centers of the k-Means clusters for various values of k and use the Elbow method to pick the best value of k.\n",
    "* Use the Silhouette Coefficient method to determine the best value of k as well.\n",
    "* Use the best value(s) of k to cluster the prepared bank dataset with k-means. Aim at producing meaningful clustering.\n",
    "* Use MDS and t-SNE for visualising the clusterings. Write a short conclusion about the characteristics of the clusters (max 250 words in a markdown cell).\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Task 3: DBSCAN"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Apply the DBSCAN clustering algorithm to the prepared bank dataset.\n",
    "* Use MDS and t-SNE for visualising the clustering. Write a short conclusion about the characteristics of the clusters and compare the performance of DBSCAN to k-Means. (max 250 words in a markdown cell)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": ".venv",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
